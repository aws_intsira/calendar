var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyP = require('body-parser');
var session = require('express-session');

var indexRouter = require('./routes/home');
var usersRouter = require('./routes/users');
var signup = require('./routes/signup');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use(bodyP.urlencoded({ extended: false }));
app.use(session({
    name: 'calendar',
    secret: '2018',
    resave: false,
    saveUninitialized: false,
    cookie: {maxAge: 24 * 60 * 60 * 1000} // cookie.secure: true  On production
}));


app.use('/', indexRouter);
app.use('/admin', usersRouter);
app.use('/signup', signup);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  //Il faut exporter une variable d'environnement dans le system pour mettre en production NODE_ENV
    /*
        /etc/init/env.conf
        env NODE_ENV=production

        or process.env.NODE_ENV
     */
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
