# CALENDAR #
## Description du projet

Ce projet consiste à réaliser une application web en node.js permettant de gérer un calendrier personnel.

Pour accéder à l'application, il faut créer un compte ou entrer en mode invité `(email: guest@guest.com,password: pass)`.
    
## Installation et access local

1. Installer node.js dans votre machine au cas où c'est pas encore le cas
        
      Suivez ce tuto pour [installer node.js](https://nodejs.org/en/download/package-manager/) 

2. Installer la base de donnée mongodb (suivez ce lien pour [installer mongodb](https://docs.mongodb.com/manual/installation/))
   
      Lancer la service mongodb. Si vous êtes sous linux, lancer la commande 
      ```
           sudo service mongod start
      ``` 
      
      et créer une base de donnée nommé **calendar** (Normalement pas besoin)
      ```
           use calendar
      ```
   
3. Entrer dans le dossier source **calendar/** et installer les packages de dépendance avec la commande
    
      ```
        npm install
      ```
      
4. Lancer le serveur avec le mode _debogage_ ou pas

      ```
        npm start
      ```
      ou
      ```
        DEBUG=calendar:* npm start
      ```
    
5. Accéder à l'application sur l'addresse [localhost:3000/](localhost:3000/)

      Pour changer le port d'écoute du serveur, vous pouvez le modifier en le spécifiant dans le fichier [./bin/www](./bin/www) ou en regardant la documentation [suivant](https://docs.npmjs.com/misc/config).
      
## Lancement avec docker-compose

1. Installer docker-compose dans votre machine: [lien](https://docs.docker.com/compose/install/)

2. Changer l'addresse d'accès à la basse de donnée dans le fichier [./calendar.js](./calendar.js) : 
       ```
            var mongoDB = 'mongodb://mongo/calendar';
       ```
3. Lancez la commande suivant depuis le dossier source **calendar/**
        ```
            docker-compose up
        ```
        et 
        ```
            docker-compose down
        ``` 
        pour arreter.

Une fois les services et containers docker lancés, accéder à l'application sur l'addresse [localhost:8082/](localhost:8082/).
Au cas où le port 8082 est déjà ouvert, il faut changer le port d'écoute dans le fichier *docker-compose.yml*.