var express = require('express');
var router = express.Router();
const calendar = require('../lib').Calendar;
/* GET home page. */
router
    .get('/', function (req, res, next) {
        if (req.session.user) {
            res.render('signup', {
                title: 'Register',
                val: req.session.user,
                actionVal: 'Update your account',
                btVal: 'Update'
            });
        } else {
            res.render('signup', {title: 'Register', val: req.body, actionVal: 'Create an account', btVal: 'Sign up'});
        }
    })
    .post('/', function (req, res, next) {
        if (req.body.password === req.body.pass) {

            if (req.session.user) {
                //Update an account
                calendar.updateUser(req.session.user._id, {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: req.body.password
                }, function (err, raw) {
                    if (err) {
                        res.render('signup', {
                            title: 'Register',
                            val: req.body,
                            actionVal: 'Update your account',
                            btVal: 'Update',
                            errors: 'Bad data'
                        });
                    } else {
                        //L'objet raw n'est pas pareil que l'objet User
                        calendar.checkConnection(req.body, function (err, user) {
                            if (user) {
                                req.session.user = user;
                                req.session.name = user.name;
                                res.cookie('userName', user.lastName);
                                res.cookie('userId', user.id);
                                res.cookie('userStatus', ''+user.status);
                                res.redirect('/calendar/' + req.session.user.lastName);
                            }
                        })
                    }
                })
            } else {
                var user = calendar.addUser(req.body);
                if (user) {
                    // saved!
                    req.session.user = user;
                    req.session.name = user.name;
                    res.cookie('userName', user.lastName);
                    res.cookie('userId', user.id);
                    res.cookie('userStatus', ''+user.status);
                    res.redirect('/calendar/' + req.session.user.lastName);
                } else {
                    res.render('signup', {
                        title: 'Register',
                        val: req.body,
                        actionVal: 'Create an account',
                        btVal: 'Sign up',
                        errors: 'Bad data'
                    });
                }
            }

        } else {
            res.render('signup', {
                title: 'Register',
                val: req.body,
                actionVal: 'Create an account',
                btVal: 'Sign up',
                errors: 'The two passwords must be equal'
            });
        }
    });

module.exports = router;
