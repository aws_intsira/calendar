var express = require('express');
var router = express.Router();
const calendar = require('../lib').Calendar;

function validate(req,res,next, callback){
    if (req.session.user) {
        if (req.session.user.status === 'ADMIN') {
            //Si l'utilisateur est de status ADMIN
            callback();
        } else {
            res.send("Vous n'avez pas le droit d'administrateur");
        }
    } else {
        res.redirect('/login');
    }
}

/* GET users listing. */
router
    .get('/', function (req, res, next) {
        validate(req,res,next,function () {
            res.render('admin', {});
        })
    })
    .get('/users', function (req, res, next) {
        validate(req,res,next,function () {
            calendar.getAllUser(function (err, users) {
                if (err) {
                    res.send("Erreur" + err);
                } else {
                    res.render('adminusers', {users: users});
                }
            })
        })
    })
    .post('/users', function (req, res, next) {
        validate(req,res,next,function () {
            calendar.findUserByName(req.body.search, function (err, users) {
                if (err) {
                    res.send("Erreur" + err);
                } else {
                    res.render('adminusers', {search: req.body.search, users: users});
                }
            })
        })
    })
    .get('/users/add', function (req, res, next) {
        validate(req,res,next,function () {
            res.render('manageusers', {
                errors: [],
                actionTitle: "Ajouter un utilisateur",
                action: 'add',
                val: {}
            });
        })
    })
    .get('/users/update', function (req, res, next) {
        validate(req,res,next,function () {
            calendar.findUserById(req.query.id, function (err, user) {
                if (err) {
                    res.redirect('/admin/users');
                } else {
                    res.render('manageusers', {
                        errors: [],
                        actionTitle: "Modifiaction d'un utilisateur",
                        action: 'update',
                        val: user
                    });
                }
            })
        })
    })
    .post('/users/add', function (req, res, next) {
        validate(req,res,next,function () {
            if (req.body.password === req.body.pass) {
                var user = calendar.addUser(req.body);
                if (user) {
                    if (req.body.bt === 'Enregistrer') {
                        res.render('manageusers', {
                            errors: [],
                            actionTitle: "Modifiaction d'un utilisateur",
                            action: 'update',
                            val: user
                        });
                    } else {
                        res.render('manageusers', {
                            errors: [],
                            actionTitle: "Ajouter un utilisateur",
                            action: 'add',
                            val: {}
                        });
                    }
                } else {
                    res.render('manageusers', {
                        errors: [],
                        actionTitle: "Ajouter un utilisateur",
                        action: 'add',
                        val: req.body
                    });
                }
            } else {
                res.render('manageusers', {
                    errors: ['The two passwords must be equal'],
                    actionTitle: "Ajouter un utilisateur",
                    action: 'add',
                    val: req.body
                });
            }
        })
    })

    .post('/users/update', function (req, res, next) {
        validate(req,res,next,function () {
            calendar.updateUser(req.body.id, {status: req.body.status},function (err,raw) {
                if(err){
                    res.render('manageusers', {
                        errors: ['erreur de mise à jour'],
                        actionTitle: "Modifiaction d'un utilisateur",
                        action: 'update',
                        val: req.body
                    });
                }
            });
            if (req.body.bt === 'Enregistrer') {
                res.render('manageusers', {
                    errors: [],
                    actionTitle: "Modifiaction d'un utilisateur",
                    action: 'update',
                    val: req.body
                });
            } else {
                res.render('manageusers', {
                    errors: [],
                    actionTitle: "Ajouter un utilisateur",
                    action: 'add',
                    val: {}
                });
            }
        })
    })
    .all('/users/delete', function (req, res, next) {
        validate(req,res,next,function () {
            var id = req.query.id? [req.query.id] : req.body.checkeditem;
            if(id){
               id = Array.isArray(id)? id: [id];
               calendar.getEventByUserId(id,function (err,val) {
                   if(val){
                       var resultat=[];
                       var vide = [];
                       id.forEach(function (i) {
                           var user;
                           var content =[];
                           val.forEach(function (e) {
                               if(e.owner._id == i){
                                   user=e.owner;
                                   content.push(e);
                                   vide.push(i);
                               }
                           });
                           if(user)resultat.push({user: user,content:content});
                       });
                       calendar.getUserByArrayId(id,function (r,result) {
                           if(result){
                               result.forEach(function (ut) {
                                   if (!vide.includes(''+ut._id)){
                                       resultat.push({user:ut,content:[]})
                                   }
                               });
                               res.render('deleteusers', {
                                   id: id,
                                   resutls: resultat
                               });
                           }
                       })
                   }
               });
            } else {
                res.redirect('/admin/users');
            }
        })
    })
    .post('/users/deletemanage', function (req, res, next) {
        validate(req,res,next,function () {
            if(req.body.id){
                calendar.deleteUser(JSON.parse(req.body.id),function (err) {
                    res.redirect('/admin/users');
                })
            }else {
                res.redirect('/admin/users')
            }
        })
    });

module.exports = router;
