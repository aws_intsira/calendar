var express = require('express');
var router = express.Router();
var multer  = require('multer');
var upload = multer();
const calendar = require('../lib').Calendar;

/* GET calendar page. */
router.get('/', function (req, res, next) {
    res.redirect('/login');
});

//Pour la route /calendar
router
    .get('/calendar/:user', function (req, res, next) {
        if (req.session.user) {
            if (req.params.user != req.session.user.lastName) {
                //Si on essaie d'usurper les calendriers des autes utilisateurs
                res.redirect('/calendar/' + req.session.user.lastName);
            }
            //Random color
            req.session.rColor = req.session.rColor ? req.session.rColor : '#' + ('000000' + Math.floor(Math.random() * 16777215).toString(16)).slice(-6);
            var param = {
                title: 'Calendar',
                rColor: req.session.rColor,
                rText: ("" + req.session.user.firstName).slice(0, 2).toUpperCase(),
                name: req.session.name,
                status: req.session.user.status
            }
            res.render('home', param);
        } else {
            res.redirect('/login');
        }
    })
    .post('/calendar/:user/management/:action',upload.none(), function (req, res, next) {
        if (req.session.user && req.params.user === req.session.user.lastName) {
            switch (req.params.action) {
                case 'save':
                    var val = req.body;
                    delete val.eId;
                    val.owner = req.session.user._id;
                    calendar.addEvent(val,function (err) {
                        if (err){
                            res.send({r: false});
                        } else {
                            res.send({r: true});
                        }
                    });
                    break;
                case 'update':
                    var val = req.body.eId;
                    delete req.body.eId;
                    calendar.updateEvent(val,req.body,function (err) {
                        if (err){
                            res.send({r: false});
                        } else {
                            res.send({r: true});
                        }
                    });
                    break;
                case 'delete':
                    calendar.deleteEvent(req.body.eId,function (err) {
                        if (err){
                            res.send({r: false});
                        } else {
                            res.send({r: true});
                        }
                    });
                    break;
                default:
                    res.send({});
            }
        } else {
            res.send({});
        }
    })
    .post('/calendar/:user/json', function (req, res, next) {
        if (req.session.user) {
            switch (req.body.action) {
                case 'events':
                    calendar.getAllEvent(function (err, doc) {
                        if(err){
                            res.json({});
                        }else {
                            res.json(doc);
                        }
                    });
                    break;
                case 'all':
                    res.json({});
                default:
                    res.json({});
            }
        } else {
            res.json({});
        }
    });

//Gestion de login
router
    .get('/login', function (req, res, next) {
        if (req.session.user) {
            res.redirect('/calendar/' + req.session.user.lastName);
        } else {
            res.render('login', {title: 'Login'});
        }
    })
    .post('/login', function (req, res, next) {
        calendar.checkConnection(req.body, function (err, user) {
            if (user) {
                req.session.user = user;
                req.session.name = user.name;
                res.cookie('userName', user.lastName);
                res.cookie('userId', user.id);
                res.cookie('userStatus', ''+user.status);
                //Tester si le compte est encore valide
                if(user.status === 'NOT_AVAILABLE') {
                    res.send("Ton compte n'est plus valide!!");
                }else {
                    calendar.userConnected.push(user);
                    res.redirect('/calendar/' + req.session.user.lastName);
                }
            } else {
                res.render('login', {title: 'Login', errors: true, email:req.body.email, password: req.body.password});
            }
        });
    });

router.get('/logout', function (req, res, next) {
    req.session.user = null;
    res.clearCookie('userName');
    res.clearCookie('userId');
    res.clearCookie('userStatus');
    res.redirect('/login');
});

module.exports = router;
