const calendar = require('./calendar');
const User = calendar.User;
const Event = calendar.Event;

class Calendar {
    constructor() {
        this.userConnected = ['test'];//Liste des utilisateurs connectés
    }

    addUser(req) {
        var user = new User({
            firstName: req.firstName,
            lastName: req.lastName,
            email: req.email,
            password: req.password,
            status: req.status ? req.status : 'AVAILABLE'
        });
        user.save(function (err) {
            if (err) {
                console.log(err);
                errorHandler(err);
            }
        });
        this.userConnected.push(user);//Ajouter l'utilisateur comme connecté
        return user;
    }

    updateUser(id, req, callback) {
        User.updateOne({_id: id}, req, callback);
    }

    getAllUser(callback) {
        User.find({}).exec(callback);
    }

    findUserByName(name, callback) {
        User.find({firstName: {$regex: '.*' + name + '.*'}}).exec(callback);
    }

    findUserById(id, callback) {
        User.findOne({_id: id}, callback);
    }
    getUserByArrayId(id, callback){
        User.find({_id: {$in: id}}).exec(callback);
    }

    deleteUser(id,callback) {
        //Suprimer tous les évènement de cet utilisateur
        Event.deleteMany({owner:{$in: id}}).exec();
        User.deleteMany({_id:{$in: id}}).exec(callback);
    }

    addEvent(req, callback) {
        var x = new Event(req);
        x.save(callback);
    }

    updateEvent(idEvent, req, callback) {
        Event.updateOne({_id: idEvent}, req, callback);
    }

    deleteEvent(idEvent, callback) {
        Event.deleteOne({_id: idEvent}, callback);
    }

    getAllEvent(callback) {
        Event.find({}).populate('owner', 'firstName lastName status').exec(callback);
    }

    getEventByUserId(id, callback) {
        Event.find({owner: {$in: id}}).populate('owner').exec(callback);
        //PROBLEME DE SYNCHRONISATION
        /*var res = [];
        id.forEach(function (i) {
            User.findOne({_id: i}).then(user => {
                Event.find({owner: i}).then(event => {
                    res.push({user: user, content: event});
                })
            });
        });
        return res;*/
    }

    checkConnection(req, callBack) {
        User.findOne({email: req.email, password: req.password}, callBack);
    }
}

const c = new Calendar();

//Création de l'utilisateur ADMIN
let admin = {
    firstName: 'Admin',
    lastName: 'Admin',
    email: 'marlino.rasoloniaina@ens.uvsq.fr',
    password: 'admin',
    status: 'ADMIN'
};

//Création de l'utilisateur INVITE
let invite = {
    firstName: 'Invite',
    lastName: 'Guest',
    email: 'guest@guest.com',
    password: 'pass',
    status: 'INVITE'
};

User
    .count({firstName: 'admin', lastName: 'admin'}, function (err, count) {
        if (err) handleError(err);
        if (count === 0) {
            c.addUser(admin);
        }
    })
    .count({firstName: 'calendar', lastName: 'calendar'}, function (err, count) {
        if (err) handleError(err);
        if (count === 0) {
            c.addUser(invite);
        }
    });

exports.Calendar = c;