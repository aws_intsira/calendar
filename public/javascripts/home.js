/**
 * http://usejsdoc.org/
 */
$(function () {

    function getCookie(name) {
        function escape(s) {
            return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1');
        };
        var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
        return match ? match[1] : '';
    }

    //internationalisation
    var lang = navigator.language.split("-")[0];

    /*________________Bootstrap-datepicker____________________*/
    var datePicker = $('#datepicker');

    datePicker.datepicker({
        language: lang,
        todayHighlight: true,
        immediateUpdates: true,
        templates: {
            leftArrow: '<i class="fa fa-long-arrow-left"></i>',
            rightArrow: '<i class="fa fa-long-arrow-right"></i>'
        },
    })
        .datepicker('setDate', new Date());

    /****************************************************************************************
     ================================Gestion des actions avec WebSocket======================
     ***************************************************************************************/
        // Utility function
        //const send = (ws, data) => ws.send(JSON.stringify(data));
    var containerEl = $('#fullcalendar');
    const ws = new WebSocket('ws://' + window.location.host);

    ws.addEventListener('open', function(e) {
        ws.addEventListener('message', function(e) {
            const parsed = JSON.parse(e.data);
            switch (parsed.type) {
                case 'fetchEvent':
                    //Refetches events from all sources and rerenders them on the screen
                    containerEl.fullCalendar('refetchEvents');
                    break;
                case 'error':
                    console.error(parsed.message);
                    break;
                default:
                    console.error('Bad message', parsed);
            }
        });
    });
    /*___________________FullCallendar______________________*/

    var myModal = $('#myModal');
    var btRemove = $("#myModal .modal-footer button[name='remove']");
    var btUpdate = $("#myModal .modal-footer button[name='update']");
    var eTitle = $('#eTitle');
    var eStart = $('#eStart');
    var eEnd = $('#eEnd');
    var eUrl = $('#eUrl');
    var eColor = $('#eColor');
    var eTextColor = $('#eTextColor');
    var eId = $('#eId');
    var myPopover = $('#myPopover');
    var myDelete = $("#myDelete");

    containerEl.fullCalendar({
        header: false,
        locale: lang,
        defaultDate: new Date(),
        now: new Date(),
        height: (function () {
            return window.innerHeight - 114;
        }),
        nowIndicator: true,
        slotDuration: '00:15:00',
        weekNumbers: true,
        selectable: true,
        selectHelper: true,
        selectOverlap: false,//the user will not be allowed to select periods of time that intersect
        // with events on the calendar
        eventOverlap: function(stillEvent, movingEvent){
            //Determines if events being dragged and resized are allowed to overlap each other.
            return false;
        },
        eventAllow:function(dropInfo, draggedEvent){
            //If specified, it must return either true or false as to whether the calendar will allow the
            // given event (draggedEvent) to be dropped at the given location (dropInfo).
            if(draggedEvent.owner) {
                if (getCookie('userId') !== draggedEvent.owner._id) {
                    if('ADMIN' !== getCookie('userStatus'))
                    //Si l'évènement n'appartient à l'utilisateur
                        return false;
                }
            }else {
                return false;
            }
        },
        eventDrop:function( event, delta, revertFunc, jsEvent, ui, view ) {
            let formData = new FormData();
            formData.append('start',$.fullCalendar.moment(event.start).format());
            formData.append('end',$.fullCalendar.moment(event.end).format());
            formData.append('eId',event._id);
            myAction('update',formData ,function (doc) {
                if(doc.r){
                    //Refetches events from all sources and rerenders them on the screen
                    //containerEl.fullCalendar('refetchEvents');
                    ws.send(JSON.stringify({type: 'notification'}));
                    //hide modal
                    $('#myModal .modal-header button').click();
                    containerEl.fullCalendar('unselect');
                }else {
                    //Si l'enregistrement n'est pas bien effectué
                }
            });
        },
        eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) {
            let formData = new FormData();
            formData.append('start',$.fullCalendar.moment(event.start).format());
            formData.append('end',$.fullCalendar.moment(event.end).format());
            formData.append('eId',event._id);
            myAction('update',formData ,function (doc) {
                if(doc.r){
                    //Refetches events from all sources and rerenders them on the screen
                    //containerEl.fullCalendar('refetchEvents');
                    ws.send(JSON.stringify({type: 'notification'}));
                    //hide modal
                    $('#myModal .modal-header button').click();
                    containerEl.fullCalendar('unselect');
                }else {
                    //Si l'enregistrement n'est pas bien effectué
                }
            });
        },
        editable: true,
        eventLimit: true, // allow "more" link when too many events [when too many events in a day, show the popover]
        select: function (start, end, jsEvent, view) {
            //Callback for selectable
            //Edit Event
            $('#myModal .modal-title').text("Edit Event");
            btUpdate.text("Validate");
            btRemove.removeClass("visible").addClass("invisible");
            eStart.val(start.format());
            eEnd.val((end.format() === 'Invalid date') ? '' : end.format());
            $('#eTitle, #eUrl, #eId').val("");
            myModal.modal();
        },
        eventClick: function (calEvent, jsEvent, view) {
            try {
                //Update Event
                if(getCookie('userId') === calEvent.owner._id || 'ADMIN' === getCookie('userStatus')){
                    //Si l'évènement appartient à l'utilisateur
                    myPopover.popover("dispose");
                    $('#myModal .modal-title').text("Manage Event");
                    btUpdate.text("Update");
                    btRemove.text("Delete");
                    btRemove.addClass("visible").removeClass('invisible');
                    eTitle.val(calEvent.title);
                    eStart.val($.fullCalendar.moment(calEvent.start).format());
                    var end = $.fullCalendar.moment(calEvent.end).format();
                    eEnd.val((end === 'Invalid date') ? '' : end);
                    eUrl.val(calEvent.url);
                    eColor.val(calEvent.color);
                    eTextColor.val(calEvent.textColor);
                    eId.val(calEvent._id);
                    myModal.modal();
                }
                if (calEvent.url) {
                    return false;
                }
            }catch (e) {
                
            }
            
        },
        eventMouseover: function (calEvent, jsEvent, view) {
            myPopover.popover("dispose");
            try {
                if(getCookie('userId') === calEvent.owner._id || 'ADMIN' === getCookie('userStatus')){
                    myPopover.popover({
                        title: "<small>Owner</small>",
                        //content: "<a href=\"" + calEvent.url + "\" onclick=\"window.open(this.href,'_blank'); return false;\" class=\"btn btn-link\">Link</a>",
                        content: "<p class=\"small\">"+calEvent.owner.firstName.toUpperCase()+" "+calEvent.owner.lastName+"</p>",
                        html: true,
                        animation: true,
                        trigger: "click",
                        delay: {hide: 1000},
                        placement: "bottom"
                    });
                    myPopover
                        .popover('show')
                        .css({"top": jsEvent.clientY + "px", "left": jsEvent.clientX + "px"});
                    $('.popover').css({"transform": "translate3d(" + jsEvent.clientX + "px, " + jsEvent.clientY + "px, 0px)"});
                    myPopover.click();
                }
            }catch (e) {
                
            }

        },
        eventMouseout: function (calEvent, jsEvent, view) {
            //Show the Event URL
            myPopover.click();
        },
        // http://fullcalendar.io/docs/google_calendar/
        googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
        eventSources: [
            {
                events: function (start, end, timezone, callback) {
                    $.ajax({
                        url: '/calendar/' + getCookie('userName') + '/json',
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            action: 'events'
                        },
                        success: function (doc) {
                            callback(doc);
                        }
                    });
                }
            },
            {
                // US Holidays
                googleCalendarId: 'en.usa#holiday@group.v.calendar.google.com'
            },

        ]
    });
    /*____________________Remplissage du champ select pour visualiser les views, Today et Titre_____________________*/

    var buttonText = {};

    function updateTitle() {
        //Mise à jour du titre
        $('#mytitle').text(containerEl.fullCalendar('getView').title);
    }

    if ('buttonText' in $.fullCalendar.locales[lang] === false) {
        buttonText = {year: "Year", month: "Month", week: "Week", day: "Day", list: "List", today: "Today"};
    } else {
        buttonText = $.fullCalendar.locales[lang].buttonText;
    }
    $.each(buttonText, function (name, viewname) {
        if (name != 'prev' && name != 'next' && name != 'today' && name != 'year') {
            $("select[name='view']").append(
                $('<option/>')
                    .attr('value', name)
                    .prop('selected', name == 'month')
                    .text(viewname)
            );
        }
    });
    $('#mytoday').text(buttonText.today);
    updateTitle();

    /*____________________Gestion des évènements_____________________*/

    function updateDatePicker() {
        datePicker.datepicker('destroy');
        datePicker.datepicker('update', containerEl.fullCalendar('getDate').toDate());
    }

    //changement de vue
    $("select[name='view']").on('change', function () {
        switch (this.value) {
            case "week":
                containerEl.fullCalendar('changeView', 'agendaWeek');
                break;
            case "day":
                containerEl.fullCalendar('changeView', 'agendaDay');
                break;
            case "list":
                containerEl.fullCalendar('changeView', 'list');
                break;
            default:
                containerEl.fullCalendar('changeView', 'month');

        }
        ;
        updateDatePicker();
        updateTitle();
    });
    //Today
    $('#mytoday').click(function () {
        containerEl.fullCalendar('today');
        datePicker.datepicker('update', new Date());
        updateTitle();
    });

    //Prev
    $('#myprev').click(function () {
        containerEl.fullCalendar('prev');
        updateDatePicker();
        updateTitle();
    });

    //Next
    $('#mynext').click(function () {
        containerEl.fullCalendar('next');
        updateDatePicker();
        updateTitle();
    });

    /*_________Pour le date picker_____________*/
    datePicker.on('changeDate', function (e) {
        // `e` here contains the extra attributes
        $("select[name='view'] option[value='day']").prop('selected', true);
        containerEl.fullCalendar('changeView', 'agendaDay', $.fullCalendar.moment.parseZone(e.date).format('YYYY-MM-DD'));
        updateTitle();
    });

    datePicker.on('changeMonth', function (e) {
        // `e` here contains the extra attributes
        $("select[name='view'] option[value='list']").prop('selected', true);
        containerEl.fullCalendar('changeView', 'listMonth', $.fullCalendar.moment.parseZone(e.date).format('YYYY-MM-DD'));
        updateTitle();
    });

    /*_________Pour le formulaire d'édition d'évènement_____________*/
    // Title can't be blank
    eTitle.on('input', function () {
        var input = $(this);
        var is_name = input.val();
        if (is_name) {
            input.removeClass("is-invalid").addClass("is-valid");
        }
        else {
            input.removeClass("is-valid").addClass("is-invalid");
        }
    });
    eUrl.on('input', function () {
        if ($(this).val().substring(0, 4) == 'www.') {
            $(this).val('http://www.' + $(this).val().substring(4));
        }
        try {
            new URL($(this).val());
            $(this).removeClass("is-invalid").addClass("is-valid");
        } catch (e) {
            $(this).removeClass("is-valid").addClass("is-invalid");
        }
        if (!$(this).val()) {
            $(this).removeClass("is-invalid").removeClass("is-valid");
        }
    });

    // CLEARABLE INPUT
    function tog(v) {
        return v ? 'addClass' : 'removeClass';
    }

    $(document)
        .on('input', '.clearable', function () {
            $(this)[tog(this.value)]('x');
        })
        .on('mousemove', '.x', function (e) {
            $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
        })
        .on('click', '.onX', function () {
            $(this).removeClass('x onX').val('').change();
        });

    /****************************************************************************************
     ================================Gestion des actions avec WebSocket======================
     ***************************************************************************************/

    btUpdate.click(function () {
        //Update
        if (eTitle.val()) {
            //Si le titre n'est pas vide
            if (!eUrl.hasClass('is-invalid')) {
                //s'il n'y as pas d'erreur d'URL
                //Envoyer les données
                let action = btRemove.hasClass('visible')? 'update' : 'save';
                myAction(action, new FormData(document.querySelector("#myModal form")),function (doc) {
                    if(doc.r){
                        //Refetches events from all sources and rerenders them on the screen
                        //containerEl.fullCalendar('refetchEvents');
                        ws.send(JSON.stringify({type: 'notification'}));
                        //hide modal
                        $('#myModal .modal-header button').click();
                        containerEl.fullCalendar('unselect');
                    }else {
                        //Si l'enregistrement n'est pas bien effectué
                    }
                });
            }
        } else {
            eTitle.removeClass("is-valid").addClass("is-invalid");
        }
    });

    $("#myDelete .modal-footer button[name='btYes']").click(function () {
        //Supprimer un évènement
        myAction('delete', new FormData(document.querySelector("#myModal form")),function (doc) {
            if(doc.r){
                //Refetches events from all sources and rerenders them on the screen
                //containerEl.fullCalendar('refetchEvents');
                ws.send(JSON.stringify({type: 'notification'}));
                //hide modal
                $("#myDelete .modal-footer button[name='btNo']").click();
                $('#myModal .modal-header button').click();
                containerEl.fullCalendar('unselect');
            }else {
                //Si l'enregistrement n'est pas bien effectué
            }
        });
    });
    /****************************************************************************************/

    btRemove.click(function () {
        $(this).addClass('invisible');
        $('#myDelete .modal-title').text(eTitle.val());
        myDelete.modal();
    });

    myDelete.on('show.bs.modal', function () {
        myModal.addClass('invisible');
    });

    myDelete.on('hidden.bs.modal', function () {
        myModal.removeClass('invisible');
        btRemove.removeClass('invisible');
    });


    /****************************************************************************************
     ================================Gestion des actions avec AJAX===========================
     ***************************************************************************************/

    function myAction(action, data, callback) {
        $.ajax({
            url: '/calendar/' + getCookie('userName') + '/management/' + action,
            type: 'POST',
            data: data,
            processData: false,  // dire à jQuery de ne pas traiter les données
            contentType: false,   // dire à jQuery de ne pas définir le contentType
            success: callback
        });
    }

    /****************************************************************************************
     ================================ADMIN===========================
     ***************************************************************************************/
    $("#menubt").click(function () {
        $("#confg").modal('show');
    });
});