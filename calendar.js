//Import the mongoose module DBAL
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//Set up default mongoose connection
var mongoDB = 'mongodb://localhost/calendar';
mongoose.connect(mongoDB);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function () {
    // we're connected!

});

var eventSchema = Schema({
    title: {type: String, required: true, max: 100},
    allDay: Boolean,
    start: {type: String, required: true},//J'ai mis la date au format String pour éviter le problème avec fullCallendar
    end: String,
    url: String,
    className: String,
    editable: Boolean,
    startEditable: Boolean,
    durationEditable: Boolean,
    resourceEditable: Boolean,
    rendering: String,
    overlap: Boolean,
    constraint: String,
    color: String,
    backgroundColor: String,
    borderColor: String,
    textColor: String,
    owner: {type: Schema.Types.ObjectId, ref: 'User', required: true}
});

var userSchema = Schema({
    firstName: {type: String, required: true, max: 50},
    lastName: {type: String,lowercase:true, required: true, max: 50},
    email: {type: String, required: true, max: 100},
    password: {type: String, required: true, max: 20},
    status: {type: String, enum:['ADMIN','INVITE','AVAILABLE','NOT_AVAILABLE']}
});
// Virtual for author's full name
userSchema
    .virtual('name')
    .get(function () {
        return this.firstName.toUpperCase() + ' ' + this.lastName.charAt(0).toUpperCase()+this.lastName.slice(1);
    });

var User = mongoose.model('User', userSchema);
var Event = mongoose.model('Event', eventSchema);

exports.User = User;
exports.Event = Event;